class CreatePost2s < ActiveRecord::Migration[6.0]
  def change
    create_table :post2s do |t|
      t.string :title
      t.text :content
      t.boolean :is_available
      t.string :user

      t.timestamps
    end
  end
end
