json.extract! post2, :id, :title, :content, :is_available, :user, :created_at, :updated_at
json.url post2_url(post2, format: :json)
