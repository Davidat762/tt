Rails.application.routes.draw do
  
  get 'bmi/index'
  get "bmi", to: "bmi#index"
  post "bmi/result", to: "bmi#result"
  #get "/posts", to: "posts#index"
  #get "/posts/:id", to: "posts#show"

  #get "/users", to: redirect('https://www.google.com')
  get "/users", to: "users#index"
  get "/hello_world", to: "pages#hello"

  resources :post2s
  resources :posts
  resources :users


  





  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
